<?php
/**
 * @file
 * test_features.features.inc
 */

/**
 * Implements hook_views_api().
 */
function test_features_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function test_features_node_info() {
  $items = array(
    'delivery' => array(
      'name' => t('Order'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Order ID'),
      'help' => '',
    ),
    'products' => array(
      'name' => t('Products'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Product ID'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
